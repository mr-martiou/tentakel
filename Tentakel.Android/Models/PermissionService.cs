﻿using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tentakel.Models;
using Tentakel.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(Tentakel.Droid.Models.PermissionService))]

namespace Tentakel.Droid.Models
{
    public class PermissionService : IPermissionService
    {
        private static int REQUEST_CODE_NOTIF = 777;

        public async Task<bool> CheckPermissions()
        {
            return (
                await CheckStoragePermission()
                //&& await CheckCameraPermission()
            );
        }

        public async Task<bool> CheckCameraPermission()
        {
            try
            {
                var permissionStatus = await Permissions.CheckStatusAsync<Permissions.Camera>();
                if (permissionStatus == PermissionStatus.Granted)
                    return true;

                var newStatus = await Permissions.RequestAsync<Permissions.Camera>();
                return (newStatus == PermissionStatus.Granted);
            }
            catch (Exception ex) {

                var errorMessage = ex.Message;

                if (ex.InnerException != null)
                    errorMessage += ex.InnerException.Message;

                Print.R(errorMessage);

                throw new Exception(errorMessage);
            }
        }

        public async Task<bool> CheckStoragePermission()
        {
            var thisContext     = Forms.Context;
            var thisActivity    = Forms.Context as Activity;

            if( thisActivity == null ) 
                throw new ArgumentNullException(nameof(thisActivity));

            String[] storage_permissions = new String[] {
                Manifest.Permission.WriteExternalStorage,
                Manifest.Permission.ReadExternalStorage
            };

            String[] storage_permissions_33 = new String[] {
                Manifest.Permission.PostNotifications,
                Manifest.Permission.ReadMediaImages,
                Manifest.Permission.ReadMediaVideo,
                Manifest.Permission.ReadMediaAudio,
                Manifest.Permission.WriteExternalStorage
            };

            string[] permissions;
            var allGood = true;

            try
            {
                permissions = (Build.VERSION.SdkInt == BuildVersionCodes.Tiramisu)
                ? storage_permissions_33
                : storage_permissions;

                ActivityCompat.RequestPermissions(thisActivity, permissions, REQUEST_CODE_NOTIF);

                return allGood;
            }
            catch (Exception e)
            {
                var errorMessage = (e.InnerException != null)
                    ? "[inner] " + e.InnerException.Message
                    : e.Message;

                throw new Exception($"[PermissionService] : {errorMessage}");
            };
        }
    }
}