﻿using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using Android.Widget;

namespace Tentakel.Droid
{
    [Activity(Label = "Tentakel", Icon = "@mipmap/ic_launcher", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize )]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private readonly int REQUEST_CODE_NOTIF = 777;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            ZXing.Net.Mobile.Forms.Android.Platform.Init();

            var ttkRed      = new Android.Graphics.Color(132, 27, 27);
            var ttkGreen    = new Android.Graphics.Color(13, 50, 24);
            var ttkBrown    = new Android.Graphics.Color(65, 55, 55);

            Window.SetNavigationBarColor(ttkRed);
            Window.SetStatusBarColor(ttkRed);

            LoadApplication(new App());
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            if(requestCode == REQUEST_CODE_NOTIF)
            {
                if (grantResults[0] == Permission.Granted)
                    Toast.MakeText(this, "ALLOWED", ToastLength.Short);
                else
                    Toast.MakeText(this, "DENIED", ToastLength.Short);
            }
        }
    }
}