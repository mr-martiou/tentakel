﻿using System;
using System.IO;
using SQLite;
using Xamarin.Forms;
using Tentakel.Droid;
using System.Linq;
using Android.Content.Res;
using Tentakel.Models;

[assembly: Dependency(typeof(SQLiteDb))]

namespace Tentakel.Droid
{
    public class SQLiteDb : ISQLiteDb
    {
        // Idéalement, à partir d'un serveur à la place
        private AssetManager _assetManager = Android.App.Application.Context.Assets;

        private string _dbNotFoundMsg = "La bdd source n'existe pas...";
        private const string _DbName = "TtkSQLite_off.db3";

        // Chemin vers le fichier de base de données dans le dossier de données spécifique à l'application
        private static string _documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        private string _outputPath = Path.Combine(_documentsPath, _DbName);

        public SQLiteAsyncConnection GetConnectionAsync()
        {
            try
            {
                if (!File.Exists(_outputPath))
                {
                    Print.R($"{_outputPath} à créer.");

                    if (!_assetManager.List("").Contains(_DbName))
                    {
                        throw new FileNotFoundException(_dbNotFoundMsg);
                    }

                    // Copie le fichier de base de données depuis le dossier "Assets" vers le dossier de données spécifique à l'application
                    /*
                    */
                    using (var inputStream = _assetManager.Open(_DbName))
                    using (var outputStream = File.Create(_outputPath))
                    {
                        inputStream.CopyTo(outputStream);
                    }
                }
                else
                    Print.R($"{_outputPath} existant.");

                return new SQLiteAsyncConnection(_outputPath);
            }
            catch (Exception ex)
            {
                var errorMessage = ex.Message;

                if (ex.InnerException != null)
                    errorMessage += ex.InnerException.Message;

                throw new Exception(errorMessage);
            }
        }
    }
}