﻿using SQLite;
using Xamarin.Forms;

namespace Tentakel.Services
{
    public class DatabaseService
    {
        private static SQLiteAsyncConnection _connectionAsync;

        private static readonly object _lock = new object();

        public DatabaseService() {}

        public static SQLiteAsyncConnection ConnectionAsync { 
            get { 
                lock (_lock)
                {
                    if( _connectionAsync == null )
                        _connectionAsync = DependencyService.Get<ISQLiteDb>().GetConnectionAsync();
                }
                return _connectionAsync; 
            } 
        }
    }
}
