﻿using System.Collections.Generic;

namespace Tentakel.Services
{
    public interface IJsonService
    {
        List<T> GetJsonTableList<T>(string resourcePath);
    }
}