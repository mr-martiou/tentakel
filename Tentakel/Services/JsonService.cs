﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Tentakel.Services
{
    public class JsonService : IJsonService
    {
        List<T> IJsonService.GetJsonTableList<T>(string resourcePath)
        {
            try
            {
                List<T> tableJsons;

                using (Stream fileFeed = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourcePath))
                {
                    if (fileFeed == null)
                    {
                        throw new FileNotFoundException($"Le fichier JSON incorporé '{resourcePath}' n'a pas été trouvé.");
                    }

                    using (StreamReader reader = new StreamReader(fileFeed))
                    {
                        string jsonContent = reader.ReadToEnd();

                        if (string.IsNullOrEmpty(jsonContent))
                        {
                            throw new InvalidOperationException($"Le fichier JSON incorporé '{resourcePath}' est vide.");
                        }

                        tableJsons = JsonConvert.DeserializeObject<List<T>>(jsonContent);

                        if (tableJsons == null)
                        {
                            throw new InvalidOperationException($"Le fichier JSON incorporé '{resourcePath}' n'est pas un tableau JSON valide.");
                        }
                    }
                }

                return tableJsons;
            }
            catch (Exception e)
            {
                throw new Exception($"[GetJsonTableList : {typeof(T).Name}] {e.Message}", e);
            }
        }
    }
}
