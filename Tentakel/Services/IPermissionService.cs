﻿using System.Threading.Tasks;

namespace Tentakel.Services
{
    public interface IPermissionService
    {
        Task<bool> CheckPermissions();
        Task<bool> CheckCameraPermission();
        Task<bool> CheckStoragePermission();
    }
}
