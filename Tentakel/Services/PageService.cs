﻿using Android.Content.Res;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tentakel.Models;
using Xamarin.Forms;

namespace Tentakel
{
    public interface IPageService
    {
        Task DisplayAlert(string title, string message, string ok);
        Task<bool> DisplayAlert(string title, string message, string ok, string cancel);
        Task<Page> PopAsync();
        Task<Page> PopModalAsync();
        Task PushAsync(Page page);
        Task PushModalAsync(Page page);
        IReadOnlyList<Page> GetPagesInStack();
    }

    public class PageService : IPageService
    {
        private Page MainPage
        {
            get { return Application.Current.MainPage; }
        }

        public async Task DisplayAlert(string title, string message, string ok)
        {
            await MainPage.DisplayAlert(title, message, ok);
        }

        public async Task<bool> DisplayAlert(string title, string message, string ok, string cancel)
        {
            return await MainPage.DisplayAlert(title, message, ok, cancel);
        }

        public async Task PushAsync(Page page)
        {
            await MainPage.Navigation.PushAsync(page);
        }

        public async Task<Page> PopAsync()
        {
            return await MainPage.Navigation.PopAsync();
        }

        public async Task PushModalAsync(Page page)
        {
            await MainPage.Navigation.PushModalAsync(page);
        }

        public async Task<Page> PopModalAsync()
        {
            return await MainPage.Navigation.PopModalAsync();
        }

        public IReadOnlyList<Page> GetPagesInStack()
        {
            var pagesInStack = new List<Page>(MainPage.Navigation.NavigationStack).AsReadOnly();

            
            foreach (var page in pagesInStack)
            {
                Print.R($"* ---- Page in stack ---- {page.GetType().Name}");
            }
            

            return pagesInStack;
        }
    }
}
