﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tentakel.Models;
using Tentakel.Repositories;
using Tentakel.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel.Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompareTabbedPage : TabbedPage
    {
        private Product _productA;
        private Product _productB;
        private ICompanyRepository _companyRepository = App.CompanyRepository;

        public CompareTabbedPage(Product secondProductToCompare)
        {
            InitializeComponent();

            _productB = secondProductToCompare ?? throw new ArgumentNullException(nameof(secondProductToCompare));

            // Si navigation vers la comparaison, il y a déjà un produit en cache.
            if (Application.Current.Properties.ContainsKey("productToCompareTo"))
                _productA = (Product)Application.Current.Properties["productToCompareTo"];

            if (_productA == null || _productB == null)
            {
                Application.Current.Properties["comparisonInSession"] = false;
                DisplayAlert("Comparaison", "Manque un produit pour comparaison...", "ok");
                Navigation.PopAsync();
                return;
            }

            if (_productA.Id == _productB.Id)
            {
                DisplayAlert("Comparaison", "Les produits sélectionnés pour comparaison identiques", "ok");
                Navigation.PopAsync();
                //return;
            }

            // Désactiver la barre de navigation pour cette page
            //NavigationPage.SetHasNavigationBar(this, false);

            Children.Add(new BranchPage(_productA) { IconImageSource = "branch.png", Title = "Produit A" });
            Children.Add(new BranchPage(_productB) { IconImageSource = "branch.png", Title = "Produit B" });
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var result = await HaveSharedOwnersAsync(_productA.CompanyId, _productB.CompanyId);

            compareTabbed.Title = result
                ? "Sociétés identiques."
                : "Pas de correspondances connues.";
        }

        private async Task<bool> HaveSharedOwnersAsync(int companyIdA, int companyIdB)
        {
            if (companyIdA == null || companyIdB == null)
                throw new Exception("Manque un id pour comparaison...");

            try
            {
                var listA = _companyRepository.GetOwnerList(companyIdA);
                var listB = _companyRepository.GetOwnerList(companyIdB);

                if (listA.Count < 1 || listB.Count < 1)
                    return false;

                return listA.Intersect(listB).Any();
            }
            catch(Exception ex) 
            {
                Print.R(ex.Message);
                return false;
            }
        }
    }
}