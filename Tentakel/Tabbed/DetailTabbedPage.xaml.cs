﻿using System;
using Tentakel.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel.Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailTabbedPage : TabbedPage
    {
        private ProductViewModel _viewModel;

        public DetailTabbedPage(ProductViewModel productViewModel)
        {
            _viewModel = productViewModel ?? throw new ArgumentNullException("[Onglets détails] Pas de produit (vm)... ");

            InitializeComponent();

            // Désactiver la barre de navigation pour cette page
            NavigationPage.SetHasNavigationBar(this, false);

            Children.Add(new DetailPage(_viewModel) { IconImageSource = "detail.png" });
            Children.Add(new BranchPage(_viewModel.GetProduct()) { IconImageSource = "branch.png" });
        }
    }
}