﻿using Android.Widget;
using System;
using Tentakel.Services;
using Tentakel.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel.Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainTabbedPage : TabbedPage
    {
        private IPermissionService _permissionService;

        public MainTabbedPage(IPermissionService permissionService)
        {
            _permissionService = permissionService ?? throw new ArgumentNullException(nameof(permissionService));

            InitializeComponent();

            Children.Add(new HomePage(_permissionService) { IconImageSource = "home.png" });
            Children.Add(new HistoryPage() { IconImageSource = "histo.png" });

            /* Boutons */
            var arboBtn = new ToolbarItem 
            {
                Text = "Arborescence",
                Order = ToolbarItemOrder.Secondary,
                Priority = 0
            };

            var aboutBtn = new ToolbarItem
            {
                Text = "A propos",
                Order = ToolbarItemOrder.Secondary,
                Priority = 1
            };

            arboBtn.Clicked += OnArboClick;
            aboutBtn.Clicked += OnAboutClick;

            ToolbarItems.Add(arboBtn);
            ToolbarItems.Add(aboutBtn);
        }

        private void OnArboClick(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ArboPage());
        }

        private void OnAboutClick(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new AboutPage());
        }
    }
}