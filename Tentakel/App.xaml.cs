﻿using Tentakel.CustomPages;
using Tentakel.Services;
using Tentakel.Tabbed;
using Xamarin.Forms;
using SQLite;
using Tentakel.Models;
using System;
using System.Threading.Tasks;
using Tentakel.Repositories;

namespace Tentakel
{
    public partial class App : Application
    {
        private IPermissionService _permissionService;
        private IPageService _pageService;
        private SQLiteAsyncConnection _connection;

        private static IProductRepository _productRepository;
        private static IHistoryRepository _historyRepository;
        private static ICompanyRepository _companyRepository;

        public static IProductRepository ProductRepository { get => _productRepository; }
        public static IHistoryRepository HistoryRepository { get => _historyRepository; }
        public static ICompanyRepository CompanyRepository { get => _companyRepository; }

        public App()
        {
            try {
                InitializeComponent();

                _permissionService = DependencyService.Get<IPermissionService>();
                _pageService = DependencyService.Get<IPageService>();

                _connection = DatabaseService.ConnectionAsync;

                _productRepository = new ProductRepository(_connection);
                _historyRepository = new HistoryRepository(_connection);
                _companyRepository = new CompanyRepository(_connection);

                Init();
                MainPage = new TtkNavigationPage(new MainTabbedPage(_permissionService));
            }
            catch (Exception ex) {
                var errorMessage = ex.Message;

                if (ex.InnerException != null)
                    errorMessage += ex.InnerException.Message;

                throw new Exception(errorMessage);
            }
        }

        private async Task Init()
        {
            var isInitSuccessful = true;

            if (!await _permissionService.CheckPermissions())
            {
                await _pageService.DisplayAlert("Permissions", "Tu peux lâcher l'affaire...", "ok");
                return;
            }

            if (!await TableExistsAsync<Product>(_connection))
                _connection.CreateTableAsync<Product>();
            else
                await Print.TableInfo("Product", _connection);

            if (!await TableExistsAsync<Company>(_connection))
                _connection.CreateTableAsync<Company>();
            else
                await Print.TableInfo("Company", _connection);

            if (!await TableExistsAsync<Search>(_connection))
                _connection.CreateTableAsync<Search>();
            else
                await Print.TableInfo("Search", _connection);

            if (isInitSuccessful)
                throw new Exception("Init failed, dude...");
        }

        static async Task<bool> TableExistsAsync<T>(SQLiteAsyncConnection connection)
        {
            try
            {
                var tableName = typeof(T).Name;

                Print.R($"La table {tableName} existe");
                await connection.GetTableInfoAsync(tableName);
                return true;
            }
            catch (Exception e)
            {

                Print.R(e.Message);

                if (e.InnerException != null)
                    Print.R(e.InnerException.Message);

                return false;
            }
        }

        protected override void OnStart()
        {
            
        }

        protected override void OnSleep()
        {
            // TODO : fermer la connexion lorsque l'application se termine
        }

        protected override void OnResume()
        {
        }
    }
}
