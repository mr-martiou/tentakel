﻿using Tentakel.Models;
using Xamarin.Forms;

namespace Tentakel.CustomPages
{
    public class TtkNavigationPage : NavigationPage
    {
        public TtkNavigationPage(Page root) 
            : base(root)
        {
            UpdateTitleAlignment();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            UpdateTitleAlignment();
        }

        private void UpdateTitleAlignment()
        {
            if(NavigationPage.GetHasNavigationBar(this))
            {
                var bar = GetTitleView(this);

                if(bar != null)
                {
                    bar.BackgroundColor = Constants.MainColor;
                    bar.HorizontalOptions = LayoutOptions.CenterAndExpand;
                    bar.VerticalOptions = LayoutOptions.CenterAndExpand;
                }
            }
        }
    }
}
