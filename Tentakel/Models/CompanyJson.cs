﻿namespace Tentakel.Models
{
    public class CompanyJson
    {
        public CompanyJson() { }

        public string Name { get; set; }
        public int ParentId { get; set; }
        public string LogoSrc { get; set; }
    }
}
