﻿using SQLite;
using Tentakel.Interfaces;

namespace Tentakel.Models
{
    [Table("Company")]
    public class Company : ITtkTables
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(255)]
        public string Name { get; set; }
        public int ParentId { get; set; }
        [MaxLength(255)]
        public string LogoSrc { get; set;}
    }
}
