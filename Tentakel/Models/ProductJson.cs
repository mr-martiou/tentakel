﻿namespace Tentakel.Models
{
    public class ProductJson
    {
        public ProductJson() {}

        public string Barcode { get; set; }
        public string Name { get; set; }
        public int CompanyId { get; set; }
    }
}
