﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tentakel.Models
{
    public class EventNames
    {
        public static string AnimationDone          => "AnimationDone";
        public static string ScanResult             => "ScanResult";
        public static string BackFromScan           => "BackFromScan";
        public static string ResetProduct           => "ResetProduct";
        public static string NewProduct             => "NewProduct";
        public static string HistoryNewEntry        => "HistoryNewEntry";
    }
}
