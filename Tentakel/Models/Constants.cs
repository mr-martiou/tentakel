﻿using Xamarin.Forms;

namespace Tentakel.Models
{
    public static class Constants
    {
        public const string TtaRed = "#841b1b";

        public static Color MainColor => Color.FromHex(TtaRed);
        public static bool IsTest = true;
    }
}
