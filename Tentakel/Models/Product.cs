﻿using SQLite;
using Tentakel.Interfaces;

namespace Tentakel.Models
{
    [Table("Product")]
    public class Product : ITtkTables
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(48), Unique]
        public string Barcode { get; set; }
        [MaxLength(255)]
        public string Name { get; set; }
        public int CompanyId { get; set; }
        [MaxLength(255)]
        public string Url { get; set; }
        [MaxLength(255)]
        public string ImageUrl { get; set; }
    }
}
