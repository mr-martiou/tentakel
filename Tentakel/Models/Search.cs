﻿using SQLite;
using Tentakel.Interfaces;

namespace Tentakel.Models
{
    // TO DO : Persister les recherches dans une bdd séparée.
    [Table("Search")]
    public class Search : ITtkTables
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int ProductId { get; set; }
    }
}
