﻿using SQLite;
using System;
using System.Threading.Tasks;

namespace Tentakel.Models
{
    public static class Print
    {
        public static void R(string msg)
        {
            if(Constants.IsTest)
                Console.WriteLine($"MYDEBUG: {msg}");
        }

        public static async Task TableInfo(string tableName, SQLiteAsyncConnection connection) {
            try
            {
                if (!Constants.IsTest)
                    return;

                var tableInfo = await connection.GetTableInfoAsync(tableName);

                R($"Structure de la table {tableName}:");

                foreach (var info in tableInfo)
                {
                    R($"Nom de la colonne : {info.Name}");
                    R($"Nullable : {info.notnull}");
                }
            }
            catch (Exception ex)
            {
                R($"Erreur lors de l'obtention des informations de la table : {ex.Message}");
            }
        }    
    }
}
