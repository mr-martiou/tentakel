﻿using Tentakel.Models;

namespace Tentakel.ViewModels
{
    public class ProductViewModel
    {
        public ProductViewModel(){}

        public ProductViewModel(Product product)
        {
            this.Id         = product.Id;
            this.Barcode    = product.Barcode;
            this.Name       = product.Name;
            this.Url        = product.Url;
            this.ImageUrl   = product.ImageUrl;
            this.CompanyId  = product.CompanyId;
        }

        public int Id { get; set; }
        public string Barcode { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
        public int CompanyId { get; set; }

        public Product GetProduct()
        {
            return new Product
            {
                Id = this.Id,
                Barcode = this.Barcode,
                Name = this.Name,
                Url = this.Url,
                ImageUrl = this.ImageUrl,
                CompanyId = this.CompanyId
            };
        }
    }
}
