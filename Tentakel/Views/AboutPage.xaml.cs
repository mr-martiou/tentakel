﻿using SQLite;
using System.Threading.Tasks;
using Tentakel.Models;
using Tentakel.Repositories;
using Tentakel.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private IProductRepository _productRepository;
        private ICompanyRepository _companyRepository;

        public AboutPage()
        {
            InitializeComponent();

            _connection = DatabaseService.ConnectionAsync;
            _productRepository = App.ProductRepository;
            _companyRepository = App.CompanyRepository;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await LoadProducts();
        }

        private async Task LoadProducts()
        {
            var tableProductList = await _productRepository.GetProductsAsync(10);
            var  tableProductCount = await _connection.Table<Product>().CountAsync();

            if (tableProductList != null)
            {
                productList.ItemsSource = tableProductList;
                pCount.Text = tableProductCount.ToString();
            }

            var tableCompanyList = await _companyRepository.GetCompaniesAsync(15);
            var tableCompanyCount = await _connection.Table<Company>().CountAsync();

            if (tableCompanyList != null)
            {
                companyList.ItemsSource = tableCompanyList;
                cCount.Text = tableCompanyCount.ToString();
            }

            var searchList = await _connection.Table<Search>().ToListAsync();
            var tableSearchCount = await _connection.Table<Search>().CountAsync();

            if (searchList != null)
            {
                historyList.ItemsSource = searchList;
                sCount.Text = tableSearchCount.ToString();
            }
        }
    }
}