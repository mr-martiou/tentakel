﻿using SQLite;
using System;
using Tentakel.Models;
using Tentakel.Repositories;
using Tentakel.Services;
using Tentakel.ViewModels;
using Tentakel.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScannerPage : ContentPage
    {
        private SQLiteAsyncConnection _connection = DatabaseService.ConnectionAsync;
        private IProductRepository _productRepository;
        private IHistoryRepository _histortyRepository;

        public ScannerPage()
        {
            _productRepository = App.ProductRepository;
            _histortyRepository = new HistoryRepository(_connection);

            InitializeComponent();

            MessagingCenter.Subscribe<NewProductPage, ProductViewModel>(this, EventNames.NewProduct, (npp, pvm) => {

                if(pvm == null)
                    throw new ArgumentNullException("Nouveau viewmodel null...");

                GotoDetail(pvm);
            });
        }

        private void scannerView_OnScanResult(ZXing.Result barcodeResult)
        {
            try
            {
                if(barcodeResult != null)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        overlayText.TopText = $"Code scanné : {barcodeResult.Text}";
                        Print.R($"Code scanné : {barcodeResult.Text}");
                        scannerView.IsScanning = false;

                        SearchProduct(barcodeResult.Text);
                    });
                }
            }
            catch (Exception ex)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    Print.R($"le scan n'a pas marché : {ex.Message}");

                    if(ex.InnerException != null)
                        Print.R($"****** : {ex.InnerException.Message}");

                    overlayText.TopText = $"Ca n'a pas marché : {ex.Message}";
                });
            }
        }

        /**
         * Le produit existe en base. Il est ajouté à l'historique de recherche.
         * On est redirigé vers sa page.
        */
        private async void SearchProduct(string barcode)
        {
            try
            {
                var product = await _productRepository.GetProductByBarcodeAsync(barcode);

                if (product != null)
                {
                    var productVM = new ProductViewModel(product);

                    if (await _histortyRepository.AddProductToSearchHistory(productVM.Id))
                        GotoDetail(productVM);
                    else
                        await DisplayAlert("Recherche produit", $"Redirection impossible vers {barcode}.", "Ok");
                }
                else
                {
                    await DisplayAlert("Scan result", $"Le produit {barcode} n'existe pas en base.", "Ok");

                    /*if(await DisplayAlert("Scan result", $"Le produit {barcode} n'existe pas en base.", "Créer (dev)", "Ok"))
                    {
                        await Navigation.PushModalAsync(new NewProductPage(barcode));
                    }*/
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.Message;

                if (ex.InnerException != null)
                    errorMessage += ex.InnerException.Message;

                throw new Exception(errorMessage);
            }
        }

        private void GotoDetail(ProductViewModel pvm)
        {
            Navigation.PushAsync(new AnimationPage(pvm));
        }
    }
}