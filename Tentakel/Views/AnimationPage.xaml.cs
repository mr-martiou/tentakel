﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Tentakel.Models;
using Tentakel.Tabbed;
using Tentakel.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnimationPage : ContentPage
    {
        private ProductViewModel _productViewModel;
        private bool _skipAnimation;
        private bool _returnToScan;

        public AnimationPage(ProductViewModel pvm)
        {
            _productViewModel = pvm ?? throw new ArgumentNullException("Aucun produit...");
            InitializeComponent();

            MessagingCenter.Subscribe<DetailPage>(this, EventNames.BackFromScan, (dp) =>
            {
                _returnToScan = _skipAnimation = true;
            });

            Color ttaRedColor = Color.FromHex(Constants.TtaRed);
            animLoader.Color = ttaRedColor;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            Animate();

            if(_returnToScan)
                await Navigation.PopAsync();
            else
                await GoNext();
        }

        public bool Animate()
        {
            if (_skipAnimation)
                return true;

            _skipAnimation = true;

            loaderStatus.Text = $"Animation [{_productViewModel.Barcode}]...";

            Thread.Sleep(2000);
            loaderStatus.Text = "Redirection...";
            animLoader.IsRunning = animLoader.IsVisible = false;

            Thread.Sleep(1000);

            return true;
        }

        private async Task GoNext() 
        {
            MessagingCenter.Send(this, EventNames.ScanResult);
            await Navigation.PushAsync(new DetailTabbedPage(_productViewModel));
        }
    }
}