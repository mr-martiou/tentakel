﻿using Android.Content;
using System;
using Tentakel.Models;
using Tentakel.Repositories;
using Tentakel.ViewModels;
using Tentakel.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPage : ContentPage
    {
        private ProductViewModel _productViewModel;
        private IPageService _pageService = new PageService();
        private bool _isScanOrigin;
        private IHistoryRepository _historyRepository;

        public DetailPage(ProductViewModel productViewModel)
        {
            _productViewModel = productViewModel ?? throw new ArgumentNullException("Aucun produit défini...");

            InitializeComponent();

            _historyRepository = App.HistoryRepository;

            var previousPageName = GetPreviousPageName();

            Print.R($"previousPageName = {previousPageName}");

            // Du scan vers la page animation
            _isScanOrigin = (previousPageName == "AnimationPage");

            if (string.IsNullOrWhiteSpace(_productViewModel.Name))
                _productViewModel.Name = "Non défini";

            BindingContext = _productViewModel;
        }

        protected override bool OnBackButtonPressed()
        {
            Close_Details(this, EventArgs.Empty);

            return true;
        }

        private string GetPreviousPageName()
        {
            var currentStack = _pageService.GetPagesInStack();
            if (currentStack == null)
                return "null";

            return currentStack[currentStack.Count - 1].GetType().Name;
        }

        private async void Close_Details(object sender, EventArgs e)
        {
            Print.R("Closing !");
            // On vient de la page Animation : On revient 3 pages en arrière.
            // Animation -> Scan -> Home
            if (_isScanOrigin)
            {
                MessagingCenter.Send(this, EventNames.BackFromScan);
                Print.R("Origine : Scan");
                await _pageService.PopAsync(); // supprimer celui-ci de préférence pour retour au scan au lieu de home.
            }
            else
                Print.R("Origine : Historique");

            // On vient de la page Historique : Simple PopAsync
            await _pageService.PopAsync();
        }

        private async void compareBtn_Clicked(object sender, EventArgs e)
        {
            var currentProduct = _productViewModel.GetProduct();

            var historyCount = await _historyRepository.GetHistoryCountAsync();

            Application.Current.Properties["productToCompareTo"] = currentProduct;
            await Application.Current.SavePropertiesAsync();

            var compare = await DisplayAlert("Comparaison", 
                $"Comparer {_productViewModel.Name} à un autre produit", 
                "Comparer", "Abandonner");

            if(compare)
            {
                Application.Current.Properties["comparisonInSession"] = true;
                await Application.Current.SavePropertiesAsync();

                if(historyCount < 1)
                {
                    await Navigation.PushAsync(new ScannerPage());
                }
                else
                {
                    var compareFromScan = await DisplayAlert("Comparaison",
                        $"Comment voulez-vous procéder ?",
                        "Scanner un nouveau produit", "Choisir un produit dans l'historique.");

                    if (compareFromScan)
                        await Navigation.PushAsync(new ScannerPage());
                    else
                        await Navigation.PushAsync(new HistoryPage());
                }
            }
            else
            {
                Application.Current.Properties["comparisonInSession"] = false;
                await Application.Current.SavePropertiesAsync();
            }
        }

        private void seePdtBtn_Clicked(object sender, EventArgs e)
        {
            var uri = Android.Net.Uri.Parse(_productViewModel.Url);
            var intent = new Intent(Intent.ActionView, uri);
            intent.AddFlags(ActivityFlags.NewTask);
            Android.App.Application.Context.StartActivity(intent);
        }
    }
}