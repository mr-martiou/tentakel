﻿using System;
using System.Collections.Generic;
using Tentakel.Models;
using Tentakel.Repositories;
using Tentakel.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BranchPage : ContentPage
    {
        private ICompanyRepository _companyRepository;
        private List<Company> _ownerList = new List<Company>();
        private Product _product;

        public BranchPage(Product product)
        {
            InitializeComponent();

            _product = product ?? throw new ArgumentNullException(nameof(product));

            _companyRepository = App.CompanyRepository;

            FeedPage(_product);

            branchGrid.RowSpacing = 20;
            //TestDatabaseQuery();
        }

        private void FeedPage(Product product)
        {
            if (product == null)
                throw new Exception("Un problème est survenu (produit).");

            branchPicture.Source = product.ImageUrl;

            if (product.CompanyId > 0)
            {
                // On ajoute les parent à _parentList
                _ownerList = _companyRepository.GetOwnerList(product.CompanyId); // récursif: on remonte au plus haut dans la filiation.

                if (_ownerList.Count < 1)
                    throw new Exception("Problème rencontré lors de l'ajout de propriétaire...");

                _ownerList.Reverse(); // Le dernier parent ajouté à la liste est le plus "ancien", donc le premier.

                foreach (Company company in _ownerList)
                {
                    MakeRow($"{company.Name}");
                }

                MakeRow($"{product.Name}"); // Nbr Parents + 1
            }
            else
            {
                MakeRow("Aucun propriétaire connu");
                MakeRow($"{product.Name}");
            }
        }

        private Label GetLabel(string labelText)
        {
            return new Label {
                Text = $"{labelText}",
                HorizontalTextAlignment = TextAlignment.Center,
                FontFamily = "Poppins",
                FontSize = 20
            };
        }

        private Frame GetFrame(Label label)
        {
            return new Frame {
                Content = label,
                BorderColor = Constants.MainColor,
                CornerRadius = 10,
                Padding = 10,
                Margin = new Thickness(5),
                BackgroundColor = Color.White
            };
        }

        private void MakeRow(string labelText)
        {
            var label = GetLabel(labelText);
            var frame = GetFrame(label);
            var rowIndex = branchGrid.RowDefinitions.Count;

            Grid.SetRow(frame, rowIndex);
            Grid.SetColumn(frame, 0);
            branchGrid.RowDefinitions.Insert(rowIndex, new RowDefinition { Height = GridLength.Auto });

            branchGrid.Children.Add( frame );
        }

        private async void TestDatabaseQuery()
        {
            try
            {
                Print.R("Executing TestDatabaseQuery");
                var company = await _companyRepository.GetCompanyByIdAsync(1);
                Print.R("TestDatabaseQuery result: " + (company != null ? "Success" : "Failed"));
            }
            catch (Exception ex)
            {
                Print.R("TestDatabaseQuery failed: " + ex.Message);
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Application.Current.Properties["comparisonInSession"] = false;
            Application.Current.SavePropertiesAsync();

            return base.OnBackButtonPressed();
        }
    }
}