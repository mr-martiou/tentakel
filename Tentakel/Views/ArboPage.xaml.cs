﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArboPage : ContentPage
    {
        public ArboPage()
        {
            InitializeComponent();
        }
    }
}