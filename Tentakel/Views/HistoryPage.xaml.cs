﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Tentakel.Models;
using Tentakel.Repositories;
using Tentakel.Tabbed;
using Tentakel.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryPage : ContentPage
    {
        private IHistoryRepository _historyRepository;
        private ObservableCollection<Product> _history;

        public HistoryPage()
        {
            InitializeComponent();

            _historyRepository = App.HistoryRepository;

            /*MessagingCenter.Subscribe<ScannerPage>(this, EventNames.HistoryNewEntry, (sp) =>
            {
                LoadHistory();
            });*/

            // Désactiver la barre de navigation pour cette page
            //NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            LoadHistory();
        }

        private async void historyList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            var product = (Product) e.SelectedItem; // List<Product>

            var detailNavigation    = new DetailTabbedPage(new ProductViewModel(product));
            var compareNavigation   = new CompareTabbedPage(product);

            // Déselectionner l'item
            historyList.SelectedItem = null;

            var isComparisonInSession = false;

            if (Application.Current.Properties.ContainsKey("comparisonInSession"))
                isComparisonInSession = (bool)Application.Current.Properties["comparisonInSession"];

            if(isComparisonInSession)
                Navigation.PushAsync(compareNavigation);
            else
                Navigation.PushAsync(detailNavigation);
        }

        private async void LoadHistory()
        {
            var searchResult = await _historyRepository.GetProductsInSearchHistory();

            if (searchResult != null)
            {
                _history = new ObservableCollection<Product>(searchResult); 
                historyList.ItemsSource = _history;
            }
            else
                _history = new ObservableCollection<Product>();

            searchBar.IsVisible = _history.Count > 0;
        }

        private void emptyHistoryBtn_Clicked(object sender, EventArgs e)
        {
            EmptyHistory();
        }

        private void historyList_Pulled(object sender, EventArgs e)
        {
            EmptyHistory();
            historyList.EndRefresh();
        }

        private async void EmptyHistory()
        {
            if (_history.Count < 1)
            {
                historyMsg.IsVisible = true;
                historyMsg.Text = "Rien à vider";
                return;
            }

            if( await DisplayAlert("Effacer", "Vider l'historique de recherche ?", "Oui", "Non") )
            {
                // Supprimer les entrées de l'historique de recherche
                await _historyRepository.DeleteHistory();

                // Vérifier si l'historique est vide
                var history = await _historyRepository.GetHistoryList();

                if (!history.Any())
                {
                    _history.Clear();
                }

                searchBar.IsVisible = _history.Count > 0;
                historyMsg.IsVisible = true;
                historyMsg.Text = "Historique vidé !";
            }
        }

        private async void Delete_MenuItem(object sender, EventArgs e)
        {
            var product = (sender as MenuItem).CommandParameter as Product;
            var numRows = await _historyRepository.DeleteById(product.Id);

            if (numRows > 0 )
            {
                if (historyMsg.IsVisible)
                    historyMsg.IsVisible = false;

                _history.Remove(product);
            }
            else
            {
                historyMsg.IsVisible = true;
                historyMsg.Text = $"Echec de la suppression de l'id [{product.Id}]...";
            }
        }

        private async void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            var searchText = e.NewTextValue;

            _history = new ObservableCollection<Product>(await _historyRepository.GetProductsInSearchHistory(searchText));
            historyList.ItemsSource = _history;

            UpdateHistoryView();
        }

        private void UpdateHistoryView()
        {
            // Affichage éventtuel du nombre de résultats
            var anyProductInHistory = _history.Any();
            
            if(anyProductInHistory)
            {
                historyMsg.IsVisible = false;
                historyMsg.Text = "";
            }
            else
            {
                historyMsg.IsVisible = true;
                historyMsg.Text = "Aucun résultat...";
            }
        }
    }
}