﻿using SQLite;
using System;
using Tentakel.Models;
using Tentakel.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tentakel.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewProductPage : ContentPage
    {
        private string _barcode;
        private ISQLiteDb _db;
        private SQLiteAsyncConnection _connection;

        public NewProductPage(string barcode)
        {
            _barcode = barcode ?? throw new Exception("Aucun code-barres disponible...");

            _db = DependencyService.Get<ISQLiteDb>();
            _connection = _db.GetConnectionAsync();

            InitializeComponent();
        }

        private async void creationBtn_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(newPdtName.Text))
                return;

            if( await DisplayAlert("Nouveau produit", $"Créer {newPdtName.Text} ?", "Oui", "Non") )
            {
                var product = new Product
                {
                    Barcode = _barcode,
                    Name = newPdtName.Text
                };

                var productViewModel = new ProductViewModel(product);

                await _connection.InsertAsync(product);

                MessagingCenter.Send(this, EventNames.NewProduct, productViewModel);

                await Navigation.PopModalAsync();
            }
        }
    }
}