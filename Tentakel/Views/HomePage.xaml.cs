﻿using System;
using Tentakel.Services;
using Xamarin.Forms;

namespace Tentakel
{
    public partial class HomePage : ContentPage
    {
        private IPermissionService _permissionService;

        public HomePage(IPermissionService permissionService)
        {
            _permissionService = permissionService ?? throw new ArgumentNullException(nameof(permissionService));

            InitializeComponent();

            var isComparisonInSession = false;

            if (Application.Current.Properties.ContainsKey("comparisonInSession"))
                isComparisonInSession = (bool)Application.Current.Properties["comparisonInSession"];

            toScanBtn.Source = "barcode.png";

            homeLabel.Text = isComparisonInSession 
                ? "Scannez puis comparez votre produit." 
                : "Scannez votre produit.";
        }

        private async void toScanBtn_Clicked(object sender, EventArgs e)
        {
            if(await _permissionService.CheckCameraPermission())
                await Navigation.PushAsync(new ScannerPage());
        }
    }
}