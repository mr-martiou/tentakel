﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tentakel.Models;
using Tentakel.Services;

namespace Tentakel.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private SQLiteAsyncConnection _connection;
        private List<Company> _ownerList = new List<Company>();

        public CompanyRepository(SQLiteAsyncConnection connection)
        {
            _connection = connection;
        }

        public async Task<List<Company>> GetCompaniesAsync(int? limit)
        {
            var query = $"SELECT * FROM Company ORDER BY Id ASC";

            try
            {
                if (limit != null)
                    query += $" LIMIT 0, {limit}";

                var companies = await _connection.QueryAsync<Company>(query);

                return companies;
            }
            catch (Exception e)
            {
                var errorMessage = $"{query} | {e.Message}";

                if (e.InnerException != null)
                    errorMessage = $"{query} | {e.InnerException.Message}";

                Print.R(errorMessage);

                throw new Exception(errorMessage);
            }
        }

        public async Task<Company> GetCompanyByIdAsync(int companyId)
        {
            try
            {
                var company = _connection.GetAsync<Company>(companyId).Result;

                return company;
            }
            catch (Exception e)
            {
                var errorMessage = $"{e.Message}";

                if (e.InnerException != null)
                    errorMessage = $"{e.InnerException.Message}";

                throw new Exception(errorMessage);
            }
        }

        private void AddToOwnerList(int CompanyId)
        {
            try
            {
                var parentCompany = GetCompanyByIdAsync(CompanyId).Result;

                if (parentCompany != null)
                {
                    _ownerList.Add(parentCompany);

                    if (parentCompany.ParentId > 0)
                        AddToOwnerList(parentCompany.ParentId);
                }
            }
            catch (Exception e)
            {
                var errorMessage = $"{e.Message}";

                if (e.InnerException != null)
                    errorMessage = $"{e.InnerException.Message}";

                Print.R(errorMessage);

                throw new Exception(errorMessage);
            }
        }

        public List<Company> GetOwnerList(int companyId)
        {
            _ownerList = new List<Company>();

            AddToOwnerList(companyId);

            return _ownerList;
        }

        public async Task DropCompanies()
        {
            var query = "DELETE FROM Company";

            try
            {
                _connection = DatabaseService.ConnectionAsync;
                await _connection.QueryAsync<Company>(query);
            }
            catch (Exception e)
            {
                var errorMessage = $"{query} | {e.Message}";

                if (e.InnerException != null)
                    errorMessage = $"{query} | {e.InnerException.Message}";

                Print.R(errorMessage);

                throw new Exception(errorMessage);
            }
        }
    }
}
