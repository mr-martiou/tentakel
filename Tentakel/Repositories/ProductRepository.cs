﻿using Tentakel.Models;
using System.Linq;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using SQLite;

namespace Tentakel.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private SQLiteAsyncConnection _connection;

        public ProductRepository(SQLiteAsyncConnection connection)
        {
            _connection = connection;
        }

        public async Task<List<Product>> GetProductsAsync(int? limit)
        {
            var query = $"SELECT * FROM Product ORDER BY Id ASC";
            
            try
            {
                if (limit != null)
                    query += $" LIMIT 0, {limit}";

                var products = await _connection.QueryAsync<Product>(query);

                return products;
            }
            catch (Exception e)
            {
                var errorMessage = $"{query} | {e.Message}";

                if (e.InnerException != null)
                    errorMessage = $"{query} | {e.InnerException.Message}";

                Print.R(errorMessage);

                throw new Exception(errorMessage);
            }
        }

        public async Task<Product> GetProductByBarcodeAsync(string codeBarres)
        {
            var query = $"SELECT * FROM Product WHERE Product.Barcode = '{codeBarres}'";
            
            try
            {
                var result = await _connection.QueryAsync<Product>(query);

                var product = result.FirstOrDefault();

                return product;
            }
            catch (Exception e)
            {
                var errorMessage = $"{query} | {e.Message}";

                if (e.InnerException != null)
                    errorMessage = $"{query} | {e.InnerException.Message}";

                Print.R(errorMessage);

                throw new Exception(errorMessage);
            }
        }

        public async Task<Product> GetProductByIdAsync(int productId)
        {
            var query = $"SELECT * FROM Product WHERE Product.Id = '{productId}'";

            try
            {
                var result = await _connection.QueryAsync<Product>(query);

                var product = result.FirstOrDefault();

                return product;
            }
            catch (Exception e)
            {
                var errorMessage = $"{query} | {e.Message}";

                if (e.InnerException != null)
                    errorMessage = $"{query} | {e.InnerException.Message}";

                Print.R(errorMessage);

                throw new Exception(errorMessage);
            }
        }

        /*
        public async Task ImportProducts(List<ProductJson> productsJson)
        {
            if(productsJson == null)
                throw new ArgumentNullException(nameof(productsJson));
            
            try
            {
                _asyncConnection = DatabaseService.ConnectionAsync;

                foreach (var productJson in productsJson)
                {
                    var product = new Product
                    {
                        Barcode = productJson.Barcode,
                        Name = productJson.Name,
                        CompanyId = productJson.CompanyId
                    };

                    await _asyncConnection.InsertAsync(product);

                    Print.R($"Name : {productJson.Name}" +
                        $"\nBarcode : {productJson.Barcode}" +
                        $"\nCompanyId : {productJson.CompanyId}");
                }
            }
            catch (Exception e)
            {
                if (e.Message.Contains("UNIQUE"))
                    return;

                throw new Exception($"[ImportProducts] {e.Message}");
            }
        }

        public async Task ImportCompanies(List<CompanyJson> companiesJson)
        {
            if(companiesJson == null)
                throw new ArgumentNullException (nameof(companiesJson));

            try
            {
                _asyncConnection = DatabaseService.ConnectionAsync;

                foreach (var companyJson in companiesJson)
                {
                    var company = new Company
                    {
                        ParentId = companyJson.ParentId,
                        Name = companyJson.Name,
                        LogoSrc = companyJson.LogoSrc
                    };

                    await _asyncConnection.InsertAsync(company);
                }
            }
            catch (Exception e)
            {
                if (e.Message.Contains("UNIQUE"))
                    return;

                throw new Exception($"[ImportCompanies] {e.Message}");
            }
        }
        */
    }
}
