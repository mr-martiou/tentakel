﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tentakel.Models;

namespace Tentakel.Repositories
{
    public interface ICompanyRepository
    {
        Task DropCompanies();
        Task<List<Company>> GetCompaniesAsync(int? limit);
        Task<Company> GetCompanyByIdAsync(int companyId);
        List<Company> GetOwnerList(int companyId);
    }
}