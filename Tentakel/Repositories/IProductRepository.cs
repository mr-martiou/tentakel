﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tentakel.Models;

namespace Tentakel.Repositories
{
    public interface IProductRepository
    {
        Task<Product> GetProductByBarcodeAsync(string codeBarres);
        Task<List<Product>> GetProductsAsync(int? limit);
        Task<Product> GetProductByIdAsync(int productId);
    }
}