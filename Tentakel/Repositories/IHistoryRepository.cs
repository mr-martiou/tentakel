﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tentakel.Models;

namespace Tentakel.Repositories
{
    public interface IHistoryRepository
    {
        Task<bool> AddProductToSearchHistory(int productId);
        Task<int> DeleteById(int ProductId);
        Task DeleteHistory();
        Task<List<Search>> GetHistoryList();
        Task<List<Product>> GetProductsInSearchHistory(string searchText = null);
        Task<int> GetHistoryCountAsync();
    }
}