﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tentakel.Models;
using Xamarin.Forms;

namespace Tentakel.Repositories
{
    public class HistoryRepository : IHistoryRepository
    {
        private SQLiteAsyncConnection _connection;

        public HistoryRepository(SQLiteAsyncConnection connection)
        {
            _connection = connection;
        }

        public async Task<bool> AddProductToSearchHistory(int productId)
        {
            Print.R($"InsertOrReplaceAsync de produit dans l'historique (Search) : {productId}");

            var newSearch = new Search { ProductId = productId };

            try
            {
                if (await _connection.InsertOrReplaceAsync(newSearch) > 0)
                {
                    MessagingCenter.Send(this, EventNames.HistoryNewEntry);

                    return true;
                }

                return false;
            }
            catch(SQLiteException sqlx)
            {
                var errorMessage = sqlx.Message;

                var trace = sqlx.StackTrace;

                if (sqlx.InnerException != null)
                    errorMessage += sqlx.InnerException.Message;

                throw new Exception(errorMessage);
            }
            catch (Exception ex)
            {
                var errorMessage = ex.Message;

                if (ex.InnerException != null)
                    errorMessage += ex.InnerException.Message;

                throw new Exception(errorMessage);
            }
        }

        public async Task<List<Product>> GetProductsInSearchHistory(string searchText = null)
        {
            var query = @"SELECT p.* 
                        FROM Product p 
                            INNER JOIN Search s 
                            ON p.Id = s.ProductId";

            if (searchText != null)
                query += $" WHERE p.Name LIKE '%{searchText}%'";

            //query += " ORDER BY s.Id LIMIT 0, 5";

            try
            {
                var searchHistoryList = await _connection.QueryAsync<Product>(query);

                return searchHistoryList;
            }
            catch (SQLiteException sqlx)
            {
                var errorMessage = query + " | " + sqlx.Message;

                var trace = sqlx.StackTrace;

                if (sqlx.InnerException != null)
                    errorMessage += sqlx.InnerException.Message;

                throw new Exception(errorMessage);
            }
            catch (Exception ex)
            {
                var errorMessage = query + " | " + ex.Message;

                if (ex.InnerException != null)
                    errorMessage += ex.InnerException.Message;

                throw new Exception(errorMessage);
            }
        }

        public async Task DeleteHistory()
        {
            await _connection.DeleteAllAsync<Search>();
        }

        public async Task<List<Search>> GetHistoryList()
        {
            return await _connection.Table<Search>().ToListAsync();
        }

        public async Task<int> GetHistoryCountAsync()
        {
            return await _connection.Table<Search>().CountAsync();
        }

        public async Task<int> DeleteById(int ProductId)
        {
            var query = $"DELETE FROM Search WHERE ProductId = {ProductId}";

            var modifiedRows = await _connection.QueryAsync<Search>(query);

            return modifiedRows.Count;
        }
    }
}