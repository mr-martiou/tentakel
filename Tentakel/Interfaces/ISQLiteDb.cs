﻿using SQLite;

namespace Tentakel
{
    public interface ISQLiteDb
    {
        SQLiteAsyncConnection GetConnectionAsync();
    }
}

